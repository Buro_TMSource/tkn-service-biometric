package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.*;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Component
public class StoreTasBiometricCommand implements Command {

    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.fingers}")
    private String tasFingers;
    @Value("${tkn.tas.singleFinger}")
    private String tasSingleFinger;
    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidClieRepository clieRepository;

    private static final Logger log = LoggerFactory.getLogger(StoreTasBiometricCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setDocumentId(request.getDocumentId());
        commandResponse.setId(request.getId());
        commandResponse.setScanId(request.getScanId());
        String responseDocumentManager = null;
        if (request.getRequestType().equals(RequestType.BIOM_SLAPS_REQUEST)) {
            commandResponse.setStatus(Status.BIOM_TAS_OK);
            return commandResponse;
        }
        try {
            responseDocumentManager = addMinuciasDocumentManager(request.getData(), request.getDocumentId(), request.getScanId(), request.getId());
        } catch (Exception e) {
            log.error("Error adding data to TAS with message: {}", e.getMessage());
            responseDocumentManager = null;
        }
        if (responseDocumentManager == null) {
            commandResponse.setStatus(Status.BIOM_TAS_ERROR);
        } else {
            commandResponse.setStatus(Status.BIOM_TAS_OK);
        }
        return commandResponse;
    }

    public String addMinuciasDocumentManager(String jsonRequest, String idDocumentManager, String idScan, Long operationId) throws Exception {
        JSONObject jsonObject = new JSONObject(jsonRequest);
        String li = jsonObject.optString("li", "");
        String ri = jsonObject.optString("ri", "");
        JSONObject object = addMinucias(li, ri, idDocumentManager, idScan, operationId);
        return object.toString();
    }


    public JSONObject addMinucias(String li, String ri, String idDocumentManager, String scanId, Long operationId) throws Exception {
        Map<String, String> docProperties = getMetadataMapAddress(scanId, operationId);
        JSONObject response = null;
        if (li != null && !li.isEmpty()) {
            byte[] bytes = Base64Utils.decodeFromString(li);
            docProperties.put(tasSingleFinger, "2I");
            response = tasManager.addDocument(tasFingers, idDocumentManager, null, docProperties, bytes, "application/octet-stream", "Minucia-DI.jpeg");
        }
        if (ri != null && !ri.isEmpty()) {
            byte[] bytes = Base64Utils.decodeFromString(li);
            docProperties.put(tasSingleFinger, "2D");
            response = tasManager.addDocument(tasFingers, idDocumentManager, null, docProperties, bytes, "application/octet-stream", "Minucia-DD.jpeg");
        }
        return response;
    }

    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception {
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }


    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception {
        PersonData personalData = new PersonData();
        BidClie bidClie = clieRepository.findOne(operationId);
        personalData.setPersonalNumber(String.valueOf(operationId));
        String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
        String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
        String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
        String surnames = surname + " " + surnamelast;
        personalData.setName(name);
        personalData.setSurename(surname);
        personalData.setSurenameLast(surnamelast);
        personalData.setLastNames(surnames);
        return personalData;
    }
}
