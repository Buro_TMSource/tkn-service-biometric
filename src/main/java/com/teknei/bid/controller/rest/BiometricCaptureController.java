package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.BiometricDetailDTO;
import com.teknei.bid.dto.BiometricScanRelationDTO;
import com.teknei.bid.dto.BiometricSlapsDTO;
import com.teknei.bid.util.biom.StatusHandler;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping(value = "/captureBiometric")
public class BiometricCaptureController {

    private static final Logger log = LoggerFactory.getLogger(BiometricCaptureController.class);
    @Autowired
    private BiometricController biometricController;

    @RequestMapping(value = "/capture/init/{biometricId}", method = RequestMethod.GET)
    public String initCapture(@PathVariable String biometricId) {
        StatusHandler.getInstance().initCaptureFor(biometricId);
        return "OK";
    }

    @RequestMapping(value = "/capture/status-detail/{biometricId}", method = RequestMethod.GET)
    public String getDetailFor(@PathVariable String biometricId) {
        BiometricDetailDTO dto = StatusHandler.getInstance().findDetailForBiometricId(biometricId);
        if (dto == null) {
            dto = new BiometricDetailDTO();
            dto.setLl(false);
            dto.setLr(false);
            dto.setLm(false);
            dto.setLi(false);
            dto.setLt(false);
            dto.setRl(false);
            dto.setRr(false);
            dto.setRm(false);
            dto.setRi(false);
            dto.setRt(false);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ll", dto.isLl());
        jsonObject.put("lr", dto.isLr());
        jsonObject.put("lm", dto.isLm());
        jsonObject.put("li", dto.isLi());
        jsonObject.put("lt", dto.isLt());
        jsonObject.put("rl", dto.isRl());
        jsonObject.put("rr", dto.isRr());
        jsonObject.put("rm", dto.isRm());
        jsonObject.put("ri", dto.isRi());
        jsonObject.put("rt", dto.isRt());
        return jsonObject.toString();
    }

    @RequestMapping(value = "/capture/init/{biometricId}/{ll}/{lr}/{lm}/{li}/{lt}/{rl}/{rr}/{rm}/{ri}/{rt}/{scanId}/{customerId}", method = RequestMethod.GET)
    public String initCaptureDetail(@PathVariable String biometricId, @PathVariable Boolean ll, @PathVariable Boolean lr, @PathVariable Boolean lm, @PathVariable Boolean li, @PathVariable Boolean lt, @PathVariable Boolean rl, @PathVariable Boolean rr, @PathVariable Boolean rm, @PathVariable Boolean ri, @PathVariable Boolean rt, @PathVariable String scanId, @PathVariable Long customerId) {
        StatusHandler.getInstance().initCaptureFor(biometricId);
        BiometricDetailDTO biometricDetailDTO = new BiometricDetailDTO();
        biometricDetailDTO.setLl(ll);
        biometricDetailDTO.setLr(lr);
        biometricDetailDTO.setLm(lm);
        biometricDetailDTO.setLi(li);
        biometricDetailDTO.setLt(lt);
        biometricDetailDTO.setRl(rl);
        biometricDetailDTO.setRr(rr);
        biometricDetailDTO.setRm(rm);
        biometricDetailDTO.setRi(ri);
        biometricDetailDTO.setRt(rt);
        StatusHandler.getInstance().initDetailFor(biometricId, biometricDetailDTO);
        StatusHandler.getInstance().addRelation(biometricId, scanId, customerId);
        return "OK";
    }

    @RequestMapping(value = "/capture/end", method = RequestMethod.GET)
    public String endCapture(HttpServletRequest request) {
        String biometricId = (String) request.getSession().getAttribute("biometricId");
        StatusHandler.getInstance().endCaptureFor(biometricId);
        return "OK";
    }

    private ResponseEntity<String> sendDataToServer(String biometricId) {
        BiometricScanRelationDTO dto = StatusHandler.getInstance().getScanForBiom(biometricId);
        BiometricSlapsDTO slapsDTO = StatusHandler.getInstance().getSlapsData(biometricId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "SLAPS");
        jsonObject.put("operationId", dto.getIdCustomer());
        jsonObject.put("scanId", dto.getScanId());
        jsonObject.put("ls", slapsDTO.getSlapLeft());
        jsonObject.put("rs", slapsDTO.getSlapRight());
        jsonObject.put("ts", slapsDTO.getSlapThumbs());
        return biometricController.addMinucias(jsonObject.toString(), dto.getIdCustomer(), 2);

    }

    @RequestMapping(value = {"/capture/status/", "/capture/status/{biometricId}"}, method = RequestMethod.GET)
    public String getStatusCapture(@PathVariable Optional<String> biometricId, HttpServletRequest request) {
        Integer status = null;
        if (biometricId.isPresent()) {
            status = StatusHandler.getInstance().getStatusFor(biometricId.get());
        } else {
            String biometricIdSession = (String) request.getSession().getAttribute("biometricId");
            status = StatusHandler.getInstance().getStatusFor(biometricIdSession);
        }
        if (status == null) {
            return "0";
        }
        return status.toString();
    }

    @RequestMapping(value = "/capture/status/slaps/{biometricId}/{idStatus}", method = RequestMethod.POST)
    public String setSlapsStatusProcessed(@PathVariable String biometricId, @PathVariable Integer idStatus) {
        switch (idStatus) {
            case 1:
                StatusHandler.getInstance().slapCaptureRight(biometricId);
                break;
            case 2:
                StatusHandler.getInstance().slapCaptureLeft(biometricId);
                break;
            case 3:
                StatusHandler.getInstance().slapCaptureThumbs(biometricId);
                break;
            case 4:
                StatusHandler.getInstance().slapCaptureDone(biometricId);
                break;
            case 0:
                StatusHandler.getInstance().slapCaptureZero(biometricId);
                break;
            default:
                StatusHandler.getInstance().slapCaptureZero(biometricId);
                break;
        }
        return "00";
    }

    @RequestMapping(value = {"/capture/status/slaps/{biometricId}", "/capture/status/slaps"}, method = RequestMethod.GET)
    public String getSlapStatusProcessed(@PathVariable Optional<String> biometricId, HttpServletRequest request) {
        Integer status = null;
        if (biometricId.isPresent()) {
            status = StatusHandler.getInstance().getSlapStatusFor(biometricId.get());
        } else {
            String biometricIdSession = (String) request.getSession().getAttribute("biometricId");
            status = StatusHandler.getInstance().getSlapStatusFor(biometricIdSession);
        }
        return String.valueOf(status);
    }

    @RequestMapping(value = "/capture/endError/{biometricId}", method = RequestMethod.POST)
    public ResponseEntity<String> endCaptureWithError(@PathVariable String biometricId) {
        StatusHandler.getInstance().errorCaptureFor(biometricId);
        StatusHandler.getInstance().slapCaptureDone(biometricId);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @RequestMapping(value = "/capture/data", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> setCaptureSlaps(@RequestBody String jsonSlaps) {
        JSONObject jsonObject = new JSONObject(jsonSlaps);
        String slapLeft = jsonObject.optString("slapLeft", "");
        String slapRight = jsonObject.optString("slapRight", "");
        String slapThumbs = jsonObject.optString("slapThumbs", "");
        String biometricSerialNumber = jsonObject.optString("biometricSerialNumber", "");
        BiometricSlapsDTO biometricSlapsDTO = new BiometricSlapsDTO();
        biometricSlapsDTO.setBiometricSerialNumber(biometricSerialNumber);
        biometricSlapsDTO.setSlapLeft(slapLeft);
        biometricSlapsDTO.setSlapRight(slapRight);
        biometricSlapsDTO.setSlapThumbs(slapThumbs);
        StatusHandler.getInstance().addSlapsData(biometricSerialNumber, biometricSlapsDTO);
        StatusHandler.getInstance().endCaptureFor(biometricSerialNumber);
        StatusHandler.getInstance().slapCaptureDone(biometricSerialNumber);
        return sendDataToServer(biometricSerialNumber);
    }

    @RequestMapping(value = "/capture/getData/slaps/{biometricId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getCaptureSlapsData(@PathVariable String biometricId) {
        BiometricSlapsDTO biometricSlapsDTO = StatusHandler.getInstance().getSlapsData(biometricId);
        JSONObject jsonObject = new JSONObject();
        if (biometricSlapsDTO == null) {
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.NOT_FOUND);
        }
        jsonObject.put("sl", biometricSlapsDTO.getSlapLeft());
        jsonObject.put("sr", biometricSlapsDTO.getSlapRight());
        jsonObject.put("st", biometricSlapsDTO.getSlapThumbs());
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

}