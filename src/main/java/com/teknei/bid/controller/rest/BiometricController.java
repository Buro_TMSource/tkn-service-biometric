package com.teknei.bid.controller.rest;

import com.teknei.bid.command.*;
import com.teknei.bid.dto.BiometricCaptureRequestIdentDTO;
import com.teknei.bid.util.biom.BiometricStatusSingleton;
import com.teknei.bid.util.biom.MBSSHandler;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping(value = "/biometric")
public class BiometricController {

    @Autowired
    @Qualifier(value = "biometricCommand")
    private Command biometricCommand;
    @Autowired
    private MBSSHandler mbssHandler;

    private static final Logger log = LoggerFactory.getLogger(BiometricController.class);

    @ApiOperation(value = "Uploads the biometric data related to the customer given by the url", notes = "The last parameter in the url specifies the type, 1 for fingers, 2 for slaps. The request json string must be formed as following: {ls,rs,ts} or {ll,lr,lm,li,lt,rl,rr,rm,ri,rt}. The return message includes 409 if the data is already registered for other customer, 500 if its internal server error or 200 if its ok", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The biometric data is processed correctly"),
            @ApiResponse(code = 422, message = "The biometric data could not be processes by the biometric motor")
    })
    @RequestMapping(value = "/upload/{id}/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addMinucias(@RequestBody String jsonRequest, @PathVariable Long id, @PathVariable Integer type) 
    {
    	log.info("lbl: addMinucias :"+type+ "     RequestType.BIOM_FINGERS_REQUEST: "+RequestType.BIOM_FINGERS_REQUEST);
        CommandRequest request = new CommandRequest();
        switch (type) {
            case 1:
                request.setRequestType(RequestType.BIOM_FINGERS_REQUEST);
                break;
            default:
                request.setRequestType(RequestType.BIOM_SLAPS_REQUEST);
                break;
        }
        request.setId(id);
        JSONObject jsonObject = new JSONObject(jsonRequest);
        String username = jsonObject.getString("username");
        request.setUsername(username);
        jsonRequest = jsonRequest.replace("\\\\", "\\");
        request.setData(jsonRequest);
        log.info("lbl: EXECUTE:");
        CommandResponse commandResponse = biometricCommand.execute(request);
        if (!commandResponse.getStatus().equals(Status.BIOM_OK)) {
            log.debug("Returning: {} {}", commandResponse, HttpStatus.UNPROCESSABLE_ENTITY);
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.OK);
        }
    }

    private ResponseEntity<String> parseHttpError(HttpClientErrorException he) {
        if (he.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        } else if (he.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            return new ResponseEntity<>("Peticion mal formada", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customer", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFinger(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String idReceived = jsonObject.optString("id", "99");
        String llReceived = jsonObject.optString("ll", null);
        String liReceived = jsonObject.optString("li", null);
        String lmReceived = jsonObject.optString("lm", null);
        String lrReceived = jsonObject.optString("lr", null);
        String ltReceived = jsonObject.optString("lt", null);
        String rlReceived = jsonObject.optString("rl", null);
        String riReceived = jsonObject.optString("ri", null);
        String rmReceived = jsonObject.optString("rm", null);
        String rrReceived = jsonObject.optString("rr", null);
        String rtReceived = jsonObject.optString("rt", null);
        String imageTypeReceived = jsonObject.optString("contentType", "wsq");
        if (imageTypeReceived.contains("/")) {
            String[] contents = imageTypeReceived.split("\\/");
            imageTypeReceived = contents[contents.length - 1];
        }
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFingers(llReceived, lrReceived, lmReceived, liReceived, ltReceived, rlReceived, rrReceived, rmReceived, riReceived, rtReceived, idReceived, imageTypeReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFingerAndId(@RequestBody String jsonStringRequest) 
    {
    	log.info("/search/customerId  ->>"+jsonStringRequest);
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String idReceived = jsonObject.optString("id", "1");
        String llReceived = jsonObject.optString("ll", null);
        String liReceived = jsonObject.optString("li", null);
        String lmReceived = jsonObject.optString("lm", null);
        String lrReceived = jsonObject.optString("lr", null);
        String ltReceived = jsonObject.optString("lt", null);
        String rlReceived = jsonObject.optString("rl", null);
        String riReceived = jsonObject.optString("ri", null);
        String rmReceived = jsonObject.optString("rm", null);
        String rrReceived = jsonObject.optString("rr", null);
        String rtReceived = jsonObject.optString("rt", null);
        String imageTypeReceived = jsonObject.optString("contentType", "wsq");
        if (imageTypeReceived.contains("/")) {
            String[] contents = imageTypeReceived.split("\\/");
            imageTypeReceived = contents[contents.length - 1];
        }
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFingersById(llReceived, lrReceived, lmReceived, liReceived, ltReceived, rlReceived, rrReceived, rmReceived, riReceived, rtReceived, idReceived, imageTypeReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlaps", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlaps(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String slReceived = jsonObject.optString("sl");
        if (slReceived == null || slReceived.isEmpty()) {
            slReceived = jsonObject.optString("ls");
        }
        String srReceived = jsonObject.optString("sr");
        if (srReceived == null || srReceived.isEmpty()) {
            srReceived = jsonObject.optString("rs");
        }
        String stReceived = jsonObject.optString("st");
        if (stReceived == null || stReceived.isEmpty()) {
            stReceived = jsonObject.optString("ts");
        }
        String idReceived = jsonObject.optString("id");
        if (idReceived == null || idReceived.isEmpty()) {
            idReceived = jsonObject.optString("operationId");
            if (idReceived == null || idReceived.isEmpty()) {
                Integer idOpt = jsonObject.optInt("operationId");
                if (idOpt != null) {
                    idReceived = String.valueOf(idOpt);
                }
            }
        }
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnSlaps(slReceived, srReceived, stReceived, idReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlapsId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlapsId(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String slReceived = jsonObject.optString("sl");
        String srReceived = jsonObject.optString("sr");
        String stReceived = jsonObject.optString("st");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnSlapsAndId(slReceived, srReceived, stReceived, idReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String faceReceived = jsonObject.optString("facial");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFace(idReceived, faceReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFaceId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFaceId(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String faceReceived = jsonObject.optString("facial");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFaceId(idReceived, faceReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Starts a process on biometric hardware", response = String.class)
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public String initCaptureFor(@RequestBody String serialNumber) {
        try {
            JSONObject jsonObject = new JSONObject(serialNumber);
            String serial = jsonObject.getString("biometricId");
            String operationId = jsonObject.getString("operationId");
            BiometricStatusSingleton.getInstance().initCapture(serial);
            BiometricStatusSingleton.getInstance().addRelationSerialId(serial, operationId);
            return "0";
        } catch (Exception e) {
            log.error("Error for initCapture: {} with message: {}", serialNumber, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
    @RequestMapping(value = "/status/{serialNumber}", method = RequestMethod.GET)
    public String getStatusFor(@PathVariable String serialNumber) {
        Integer foundActive = BiometricStatusSingleton.getInstance().getStatusFor(serialNumber);
        if (foundActive == 1) {
            BiometricStatusSingleton.getInstance().endCapture(serialNumber);
            String id = BiometricStatusSingleton.getInstance().getIdFromSerial(serialNumber);
            return "1|" + id;
        }
        return "0";
    }

    @ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        try {
            BiometricStatusSingleton.getInstance().initCaptureAuth(requestIdentDTO);
            return "0";
        } catch (Exception e) {
            log.error("Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureSignForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        try {
            BiometricStatusSingleton.getInstance().initCaptureSign(requestIdentDTO);
            return "0";
        } catch (Exception e) {
            log.error("Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusAuth/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BiometricCaptureRequestIdentDTO getStatusForAuth(@PathVariable String serial) {
        BiometricCaptureRequestIdentDTO requestIdentDTO = BiometricStatusSingleton.getInstance().findCaptureDataFor(serial);
        if (requestIdentDTO == null) {
            return null;
        }
        Integer status = BiometricStatusSingleton.getInstance().getStatusForAuth(requestIdentDTO);
        if (status == 1) {
            log.info("Status 1");
            BiometricStatusSingleton.getInstance().endCaptureAuth(requestIdentDTO);
            return requestIdentDTO;
        }
        return null;
    }

    @ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusSign/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BiometricCaptureRequestIdentDTO getStatusForAuthSign(@PathVariable String serial) {
        BiometricCaptureRequestIdentDTO requestIdentDTO = BiometricStatusSingleton.getInstance().findCaptureSignDataFor(serial);
        if (requestIdentDTO == null) {
            return null;
        }
        Integer status = BiometricStatusSingleton.getInstance().getStatusForSign(requestIdentDTO);
        if (status == 1) {
            log.info("Status 1 for sign");
            BiometricStatusSingleton.getInstance().endCaptureSign(requestIdentDTO);
            return requestIdentDTO;
        }
        return null;
    }


    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
    @RequestMapping(value = "/confirmAuth/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable Integer status) {
        BiometricStatusSingleton.getInstance().confirmStatus(requestIdentDTO, status);
        return "0";
    }

    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
    @RequestMapping(value = "/confirmSign/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable Integer status) {
        BiometricStatusSingleton.getInstance().confirmStatusSign(requestIdentDTO, status);
        return "0";
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/queryAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String queryCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Integer status = BiometricStatusSingleton.getInstance().queryStatus(requestIdentDTO);
        return String.valueOf(status);
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/queryAuthSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String queryCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Integer status = BiometricStatusSingleton.getInstance().queryStatusSign(requestIdentDTO);
        return String.valueOf(status);
    }

    @RequestMapping(value = "/searchDetail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<String> findDetailRecord(@PathVariable String id) {
        return mbssHandler.searchDetailById(id);
    }

    @RequestMapping(value = "/getHash/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getHashForCustomer(@PathVariable String id) {
        return mbssHandler.getHashForCustomer(id);
    }

    @RequestMapping(value = "/matchFacial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> compareFaces(@RequestBody String jsonStringRequest) {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String b64Face1 = jsonObject.optString("b64Face1");
        String b64Face2 = jsonObject.optString("b64Face2");
        Integer umbral = jsonObject.optInt("umbral");
        return mbssHandler.matchFacial(b64Face1, b64Face2, umbral);
    }

}