package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 02/08/2017.
 */
@Data
public class BiometricSlapsDTO implements Serializable {

    private String slapLeft;
    private String slapRight;
    private String slapThumbs;
    private String biometricSerialNumber;

}