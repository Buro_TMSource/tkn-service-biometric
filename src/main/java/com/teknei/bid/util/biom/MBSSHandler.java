package com.teknei.bid.util.biom;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class MBSSHandler {

    @Autowired
    private DiscoveryClient discoveryClient;

    private String mbssBaseUrl = "BID-SERV-API-MBSS";// = "http://192.168.1.101:18090";

    public static String findUrl(String serviceName, String toAppend, DiscoveryClient discoveryClient) {
        List<ServiceInstance> instanceList = discoveryClient.getInstances(serviceName);
        if (instanceList == null) {
            return null;
        }
        ServiceInstance first = instanceList.get(0);
        String foudUri = new StringBuilder(first.getUri().toString()).append("/").append(toAppend).toString();
        return foudUri;
    }

    private static final Logger log = LoggerFactory.getLogger(MBSSHandler.class);

    /**
     * Registers the fingers into Biometric DB
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base 64 String
     * @param id         - the id of the request to be saved
     * @return - 200 if success, 400 if fingers not well formed, 409 if fingers already in biometric db, 500 if error in biometric system
     */
    public ResponseEntity<String> registerFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id) {
        JSONObject requestJson = new JSONObject();
        String nStr = null;
        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
        requestJson.put("imageType", "wsq");
        requestJson.put("slaps", nStr);
        requestJson.put("id", id);
        requestJson.put("type", 1);
        return executeRequest(requestJson.toString());
    }

    /**
     * Registers the fingers in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingers(String id) {
        JSONObject fingers = MBSSHelperSingleton.getInstance().getFingers(id);
        String ll = fingers.optString("ll", null);
        String lr = fingers.optString("lr", null);
        String lm = fingers.optString("lm", null);
        String li = fingers.optString("li", null);
        String lt = fingers.optString("lt", null);
        String rl = fingers.optString("rl", null);
        String rr = fingers.optString("rr", null);
        String rm = fingers.optString("rm", null);
        String ri = fingers.optString("ri", null);
        String rt = fingers.optString("rt", null);
        return registerFingers(ll, lr, lm, li, lt, rl, rr, rm, ri, rt, id);
    }

    /**
     * Registers fingers and face in biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingersAndFace(String id) {
        String face = MBSSHelperSingleton.getInstance().getFace(id);
        JSONObject fingers = MBSSHelperSingleton.getInstance().getFingers(id);
        String ll = fingers.optString("ll", null);
        String lr = fingers.optString("lr", null);
        String lm = fingers.optString("lm", null);
        String li = fingers.optString("li", null);
        String lt = fingers.optString("lt", null);
        String rl = fingers.optString("rl", null);
        String rr = fingers.optString("rr", null);
        String rm = fingers.optString("rm", null);
        String ri = fingers.optString("ri", null);
        String rt = fingers.optString("rt", null);
        String contentType = fingers.optString("contentType", "jpeg");
        return registerFingersAndFace(ll, lr, lm, li, lt, rl, rr, rm, ri, rt, face, id, contentType);
    }

    /**
     * Registers slaps in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlaps(String id) {
        JSONObject slaps = MBSSHelperSingleton.getInstance().getSlaps(id);
        String ls = slaps.getString("ls");
        String rs = slaps.getString("rs");
        String ts = slaps.getString("ts");
        return registerSlaps(ls, rs, ts, id);
    }

    /**
     * Registers slaps and face in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlapsAndFace(String id) {
        String face = MBSSHelperSingleton.getInstance().getFace(id);
        JSONObject slaps = MBSSHelperSingleton.getInstance().getSlaps(id);
        String ls = slaps.getString("ls");
        String rs = slaps.getString("rs");
        String ts = slaps.getString("ts");
        String contentType = slaps.optString("contentType", "wsq");
        return registerSlapsAndFace(ls, rs, ts, face, id, contentType);
    }

    /**
     * Registers slaps in biometric system
     *
     * @param leftSlap
     * @param rightSlap
     * @param thumbsSlap
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlaps(String leftSlap, String rightSlap, String thumbsSlap, String id) {
        JSONObject requestJson = new JSONObject();
        String fingers = null;
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", "wsq");
        JSONObject slaps = new JSONObject();
        slaps.put("ls", leftSlap);
        slaps.put("rs", rightSlap);
        slaps.put("ts", thumbsSlap);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 4);
        return executeRequest(requestJson.toString());
    }

    /**
     * Registers slaps and face in biometric system
     *
     * @param leftSlap
     * @param rightSlap
     * @param thumbsSlap
     * @param face
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlapsAndFace(String leftSlap, String rightSlap, String thumbsSlap, String face, String id, String contentType) {
        JSONObject requestJson = new JSONObject();
        requestJson.put("facial", face);
        String fingers = null;
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", contentType);
        JSONObject slaps = new JSONObject();
        slaps.put("ls", leftSlap);
        slaps.put("rs", rightSlap);
        slaps.put("ts", thumbsSlap);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 6);
        return executeRequest(requestJson.toString());
    }


    /**
     * Registers fingers and face in biometric system
     *
     * @param ll
     * @param lr
     * @param lm
     * @param li
     * @param lt
     * @param rl
     * @param rr
     * @param rm
     * @param ri
     * @param rt
     * @param face
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingersAndFace(String ll, String lr, String lm, String li, String lt, String rl, String rr, String rm, String ri, String rt, String face, String id, String contentType) {
        JSONObject requestJson = new JSONObject();
        requestJson.put("facial", face);
        JSONObject fingers = new JSONObject();
        String slaps = null;
        fingers.put("li", li);
        fingers.put("ll", ll);
        fingers.put("lm", lm);
        fingers.put("lr", lr);
        fingers.put("lt", lt);
        fingers.put("ri", ri);
        fingers.put("rl", rl);
        fingers.put("rm", rm);
        fingers.put("rr", rr);
        fingers.put("rt", rt);
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", contentType);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 3);
        return executeRequest(requestJson.toString());
    }

    /**
     * Execute the biometric request to the server
     *
     * @param requestJson
     * @return
     */
    private ResponseEntity<String> executeRequest(String requestJson) {
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/add";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        return responseEntity;
    }

    public ResponseEntity<String> getHashForCustomer(String id) {
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/getHash/" + id;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
        log.debug("Response: {}", responseEntity);
        return responseEntity;
    }


    /**
     * Performs a search based on fingers by Id
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @param id         - the id associated
     * @return - 200 if found, 404 otherwise
     */
    public ResponseEntity<String> searchBasedOnFingersById(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id, String imageType) {
        log.info("lblancas:  searchBasedOnFingersById "+
        		"\n leftLitle::::::"+leftLitle+ 
        		"\n leftRing:::::::"+leftRing+
        		"\n leftMiddle:::::"+leftMiddle+
        		"\n leftIndex::::::"+leftIndex+
        		"\n leftThumb::::::"+leftThumb+
        		"\n rightLittle::::"+rightLittle+
        		"\n rightRing::::::"+rightRing+
        		"\n rightMiddle::::"+rightMiddle+
        		"\n rightIndex:::::"+rightIndex+
        		"\n rightThumb:::::"+rightThumb+
        		"\n id:::::::::::::"+id+
        		"\n imageType::::::"+imageType);
    	JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
        requestJson.put("imageType", imageType);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingersById";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
        	log.info("lblancas: "+
            		"\n uri    ::::::"+uri+
            		"\n entity:::::::"+entity);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
            log.info("responseEntity>>"+responseEntity);
            return responseEntity;
        } catch (HttpClientErrorException e) 
        {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Search by face and Id
     *
     * @param id
     * @param facial
     * @return
     */
    public ResponseEntity<String> searchBasedOnFaceId(String id, String facial) {
        JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("facial", facial);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/facialId";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

    public ResponseEntity<String> searchDetailById(String id) {
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/searchDetail/" + id;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Search by face
     *
     * @param id
     * @param facial
     * @return
     */
    public ResponseEntity<String> searchBasedOnFace(String id, String facial) {
        JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("facial", facial);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/facial";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Performs a search based on fingers
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @param id         - the id associated
     * @return - 200 if found, 404 otherwise
     */
    public ResponseEntity<String> searchBasedOnFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id, String imageType) {
        JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
        requestJson.put("imageType", imageType);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingers";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        return responseEntity;
    }

    public ResponseEntity<String> searchBasedOnSlaps(String leftSlap, String rightSlap, String thumbsSlap, String id) {
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/slaps";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        JSONObject internal = getSlaps(leftSlap, rightSlap, thumbsSlap);
        jsonObject.put("slaps", internal);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        return responseEntity;
    }

    public ResponseEntity<String> searchBasedOnSlapsAndId(String leftSlap, String rightSlap, String thumbsSlap, String id) {
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/slapsId";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        JSONObject internal = getSlaps(leftSlap, rightSlap, thumbsSlap);
        jsonObject.put("slaps", internal);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        return responseEntity;
    }

    public ResponseEntity<String> matchFacial(String b64face1, String b64face2, int umbral){
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/compareFacial";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("facialB641", b64face1);
        jsonObject.put("facialB642", b64face2);
        jsonObject.put("score", umbral);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        return responseEntity;
    }

    /**
     * Helper method
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @return
     */
    private JSONObject getFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb) {
        JSONObject fingersJson = new JSONObject();
        String nStr = null;
        fingersJson.put("li", leftIndex);
        fingersJson.put("lt", leftThumb);
        fingersJson.put("lm", leftMiddle);
        fingersJson.put("lr", leftRing);
        fingersJson.put("ll", leftLitle);
        fingersJson.put("ri", rightIndex);
        fingersJson.put("rt", rightThumb);
        fingersJson.put("rm", rightMiddle);
        fingersJson.put("rr", rightRing);
        fingersJson.put("rl", rightLittle);
        log.info("/search/customerId  getFingers ->>"+fingersJson);
        return fingersJson;
    }

    private JSONObject getSlaps(String leftSlap, String rightSlap, String slapThumbs) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ls", leftSlap);
        jsonObject.put("rs", rightSlap);
        jsonObject.put("ts", slapThumbs);
        return jsonObject;
    }
}
