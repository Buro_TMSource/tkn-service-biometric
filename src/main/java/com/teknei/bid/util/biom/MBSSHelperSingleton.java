package com.teknei.bid.util.biom;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MBSSHelperSingleton {

    private MBSSHelperSingleton() {
    }

    private Map<String, JSONObject> mbssHelperMap = new HashMap<>();
    private static MBSSHelperSingleton instance = new MBSSHelperSingleton();

    public static MBSSHelperSingleton getInstance() {
        return instance;
    }

    public void addFace(String id, String faceB64) {
        JSONObject jsonObjectSource = mbssHelperMap.get(id);
        if(jsonObjectSource == null){
            jsonObjectSource = new JSONObject();
        }
        jsonObjectSource.put("facial", faceB64);
        mbssHelperMap.put(id, jsonObjectSource);
    }

    public void addSlaps(String leftSlap, String rightSlap, String thumbsSlap, String id, String contentType){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ls", leftSlap);
        jsonObject.put("rs", rightSlap);
        jsonObject.put("ts", thumbsSlap);
        jsonObject.put("contentType", contentType);
        JSONObject jsonObjectSource = mbssHelperMap.get(id);
        if(jsonObjectSource == null){
            jsonObjectSource = new JSONObject();
        }
        jsonObjectSource.put("slaps", jsonObject);
        mbssHelperMap.put(id, jsonObjectSource);
    }

    public void addFingers(String id, String ll, String lr, String lm, String li, String lt, String rl, String rr, String rm, String ri, String rt, String contentType) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ll", ll);
        jsonObject.put("lr", lr);
        jsonObject.put("lm", lm);
        jsonObject.put("li", li);
        jsonObject.put("lt", lt);
        jsonObject.put("rl", rl);
        jsonObject.put("rr", rr);
        jsonObject.put("rm", rm);
        jsonObject.put("ri", ri);
        jsonObject.put("rt", rt);
        jsonObject.put("contentType", contentType);
        JSONObject jsonObjectSource = mbssHelperMap.get(id);
        if(jsonObjectSource == null){
            jsonObjectSource = new JSONObject();
        }
        jsonObjectSource.put("fingers", jsonObject);
        mbssHelperMap.put(id, jsonObjectSource);
    }

    public String getFace(String id) {
        JSONObject source = mbssHelperMap.get(id);
        if (source != null) {
            return source.optString("facial", null);
        }
        return null;
    }

    public JSONObject getSlaps(String id){
        JSONObject source = mbssHelperMap.get(id);
        if(source != null){
            return source.optJSONObject("slaps");
        }
        return null;
    }

    public JSONObject getFingers(String id){
        JSONObject source = mbssHelperMap.get(id);
        if(source != null){
            return source.optJSONObject("fingers");
        }
        return null;
    }

}
